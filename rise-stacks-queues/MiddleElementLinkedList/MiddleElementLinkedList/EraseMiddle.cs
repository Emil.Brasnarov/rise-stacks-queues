﻿namespace EraseMiddle
{
    public class EraseMiddle
    {
        static void Main(string[] args)
        {
            LinkedList<string> linkedListInput = new LinkedList<string>();

            Console.WriteLine("Please enter the number of elements:");
            int nElements = int.Parse(Console.ReadLine());

            for (int i = 0; i < nElements; i++)
            {
                string currentElement = Console.ReadLine();
                linkedListInput.AddLast(currentElement);
            }

            EraseMiddleElement(linkedListInput);
            Console.WriteLine(linkedListInput.Count);
        }

        public static LinkedList<string> EraseMiddleElement(LinkedList<string> inputLinkedList)
        {
            int lengthInput = inputLinkedList.Count;

            LinkedListNode<string> current  = inputLinkedList.First;

            for (int i = 0; i <= lengthInput / 2; i++)
            {
                current = current.Next;
                if (i == lengthInput / 2 - 1)
                    inputLinkedList.Remove(current);                    
            }
            return inputLinkedList;
        }
    }
}